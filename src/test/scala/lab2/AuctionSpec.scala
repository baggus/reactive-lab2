package lab2

import akka.actor.{ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import lab2.Auction.{Bid, Overbid, Start}
import org.scalatest.{BeforeAndAfterAll, WordSpecLike}

import scala.concurrent.duration.{Duration, _}

class AuctionSpec extends TestKit(ActorSystem("AuctionSpec"))
  with WordSpecLike with BeforeAndAfterAll with ImplicitSender {

  override def afterAll(): Unit = {
    system.terminate
  }

  "An auction" must {

    "inform current winner when he is overbid" in {

      val buyer1 = TestProbe()
      val buyer2 = TestProbe()

      val auctionRef = system.actorOf(Props(new Auction("test", Duration(5, SECONDS), Duration(5, SECONDS))))

      auctionRef ! Start

      buyer1.send(auctionRef, Bid(10))

      buyer2.send(auctionRef, Bid(20))

      buyer1.expectMsg(Overbid(20))
    }
  }

  "An auction" must {

    "not overbid the same bidder" in {

      val buyer1 = TestProbe()

      val auctionRef = system.actorOf(Props(new Auction("test", Duration(5, SECONDS), Duration(5, SECONDS))))

      auctionRef ! Start

      buyer1.send(auctionRef, Bid(10))

      buyer1.expectNoMsg()
    }
  }


  "An auction" must {

    "notify its parent when finished" in {

      val parent = TestProbe()
      val auction = parent.childActorOf(Props(new Auction("test", Duration(10, MILLISECONDS), Duration(5, MILLISECONDS))))

      auction ! Start

      parent.expectMsg(Duration(1, SECONDS), Auction.Finished("test", None))
    }
  }

  def mock(name: String, probe: TestProbe)(implicit system: ActorSystem): Unit =
    system.actorOf(ForwardActor.props(probe.ref), name)


  "An auction" must {

    "register itself in AuctionSearch" in {

      val auctionRef = system.actorOf(Props(new Auction("test", Duration(5, SECONDS), Duration(5, SECONDS))))

      val auctionSearch = TestProbe()
      auctionRef ! Auction.Start
      mock("AuctionSearch", auctionSearch)
      auctionSearch.expectMsg(AuctionSearch.Register("test"))
    }
  }
}
