package lab2

import akka.actor.{ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit, TestProbe}
import org.scalatest.{BeforeAndAfterAll, WordSpecLike}

class BuyerSpec extends TestKit(ActorSystem("BuyerSpec"))
  with WordSpecLike with BeforeAndAfterAll with ImplicitSender {

  override def afterAll(): Unit = {
    system.terminate
  }

  "A buyer" must {

    "bid afer being overbid if has enough money" in {
      val buyerRef = system.actorOf(Props(new Buyer("test", 100)))
      val auction = TestProbe()

      buyerRef ! Buyer.Start
      buyerRef ! AuctionSearch.SearchResult(List(this.testActor))

      auction.send(buyerRef, Auction.Overbid(99))

      auction.expectMsg(Auction.Bid(100))
    }
  }

  "A buyer" must {

    "not bid afer being overbid if has not enough money" in {
      val buyerRef = system.actorOf(Props(new Buyer("test", 100)))
      val auction = TestProbe()

      buyerRef ! Buyer.Start
      buyerRef ! AuctionSearch.SearchResult(List(this.testActor))

      auction.send(buyerRef, Auction.Overbid(100))

      auction.expectNoMsg()
    }
  }

}
