package lab2

import akka.actor.{Actor, ActorRef, Props}

object ForwardActor {
  def props(to: ActorRef) = Props(new ForwardActor(to))
}

class ForwardActor(to: ActorRef) extends Actor {
  override def receive = {
    case x => to forward x
  }
}