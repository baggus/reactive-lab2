package lab2

import akka.actor.ActorSystem
import akka.testkit.{ImplicitSender, TestActorRef, TestKit, TestProbe}
import lab2.AuctionSearch.{Register, Search, SearchResult, Unregister}
import org.scalatest.{BeforeAndAfterAll, WordSpecLike}

class AuctionSearchSpec extends TestKit(ActorSystem("BuyerSpec"))
  with WordSpecLike with BeforeAndAfterAll with ImplicitSender {

  override def afterAll(): Unit = {
    system.terminate
  }

  "An AuctionSearch" must {

    "register new auction" in {
      val auctionSearchRef = TestActorRef[AuctionSearch]
      auctionSearchRef ! Register("test")
      assert(auctionSearchRef.underlyingActor.registeredAuctions.contains("test"))
    }
  }

  "An AuctionSearch" must {

    "unregister an auction" in {
      val auctionSearchRef = TestActorRef[AuctionSearch]
      auctionSearchRef ! Register("test")
      auctionSearchRef ! Unregister("test")
      assert(!auctionSearchRef.underlyingActor.registeredAuctions.contains("test"))
    }
  }

  "An AuctionSearch" must {

    "return searches" in {
      val auctionSearchRef = TestActorRef[AuctionSearch]

      val seller1 = TestProbe()
      val seller2 = TestProbe()

      seller1.send(auctionSearchRef, Register("test 1"))
      seller2.send(auctionSearchRef, Register("foo"))

      auctionSearchRef ! Search("test")

      expectMsg(SearchResult(List(seller1.ref)))

      auctionSearchRef ! Search("sth")

      expectMsg(SearchResult(Nil))
    }
  }

}
