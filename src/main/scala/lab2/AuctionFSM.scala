/*
package lab2

import akka.actor.{ActorRef, Cancellable, FSM}
import lab2.AuctionFSM.{Data, State}
import lab2.AuctionMessages._

import scala.concurrent.duration.FiniteDuration

object AuctionFSM {

  //Messages
  case object BidTimerExpired

  //States
  sealed trait State

  case object Init extends State

  case object Created extends State

  case object Ignored extends State

  case object Activated extends State

  case object Sold extends State

  //Data
  sealed trait Data

  case object Uninitialized extends Data

  case class BiddingData(currentPrice: BigInt, currentWinner: ActorRef) extends Data

}

class AuctionFSM(private val bidTimer: FiniteDuration, private val deleteTimer: FiniteDuration) extends FSM[State, Data] {

  import AuctionFSM._
  import system.dispatcher

  val system = akka.actor.ActorSystem("system")

  startWith(Init, Uninitialized)

  when(Init) {
    case Event(Start, _) =>
      scheduleBidTimerExpiration()
      goto(Created)
  }

  when(Created) {
    case Event(BidTimerExpired, _) =>
      goto(Ignored)
    case Event(Bid(price), _) =>
      goto(Activated) using BiddingData(price, sender)
  }

  when(Ignored, stateTimeout = this.deleteTimer) {
    case Event(Start, _) =>
      scheduleBidTimerExpiration()
      goto(Created)
    case Event(StateTimeout, _) =>
      stop
  }

  when(Activated) {
    case Event(BidTimerExpired, data: BiddingData) =>
      data.currentWinner ! Won(data.currentPrice)
      goto(Sold)
    case Event(Bid(price), data: BiddingData) if price <= data.currentPrice =>
      sender ! BidTooLow(data.currentPrice)
      stay
    case Event(Bid(price), data: BiddingData) =>
      data.currentWinner ! Overbid(data.currentPrice)
      stay using BiddingData(price, sender)
  }

  when(Sold, stateTimeout = this.deleteTimer) {
    case Event(StateTimeout, _) =>
      stop
  }


  def scheduleBidTimerExpiration(): Cancellable = {
    system.scheduler.scheduleOnce(this.bidTimer, this.self, BidTimerExpired)
  }

  initialize()
}
*/
