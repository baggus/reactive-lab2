package lab2

import java.util.Calendar

import akka.actor._
import akka.event.LoggingReceive
import akka.persistence.{PersistentActor, RecoveryCompleted}
import lab2.AuctionSearch.Register

import scala.Option.empty
import scala.concurrent.duration.{Duration, FiniteDuration, MILLISECONDS}

private object Auction {

  //Messages

  case object Start

  case class Bid(amount: BigInt) {
    require(amount > 0)
  }

  case class Overbid(currentPrice: BigInt)

  case class Won(name: String, winningPrice: BigInt)

  case class Finished(name: String, winningPrice: Option[BigInt])

  private case object BidTimerExpired

  private case object DeleteTimerExpired

  //states
  sealed trait AuctionState

  case class Created(startTime: Long) extends AuctionState

  case object Ignored extends AuctionState

  case class Activated(currentWinner: ActorRef, currentBid: BigInt) extends AuctionState

  case object Sold extends AuctionState

  case class AuctionChangeEvent(state: AuctionState)

}

class Auction(private val name: String,
              private val bidTimer: FiniteDuration,
              private val deleteTimer: FiniteDuration) extends PersistentActor {

  import Auction._
  import system.dispatcher

  val system = akka.actor.ActorSystem("system")
  context.system.actorSelection("/user/MasterSearch") ! Register(this.name)

  val notifier = context.actorSelection("akka://Reactive2/user/Notifier")

  val calendar = Calendar.getInstance()

  var startTime = 0L

  override def persistenceId: String = "Auction " + this.name

  override def receiveCommand: Receive = init()

  override def receiveRecover: Receive = {
    case event: AuctionChangeEvent => updateState(event)
    case RecoveryCompleted =>
      if (this.startTime != 0) {
        val duration = Duration(calendar.getTimeInMillis - this.startTime, MILLISECONDS)
        if (duration > this.bidTimer) {
          self ! BidTimerExpired
        } else {
          scheduleBidTimerExpiration(this.bidTimer - duration)
        }
      }
  }

  def updateState(event: AuctionChangeEvent) = {
    context.become(
      event.state match {
        case Created(st) =>
          this.startTime = st
          created()
        case Ignored => ignored()
        case Activated(cw, cb) => activated(cw, cb)
        case Sold => sold()
      }
    )
  }

  def init(): Receive = LoggingReceive {
    case Start =>
      persist(AuctionChangeEvent(Created(calendar.getTimeInMillis))) {
        event =>
          updateState(event)
          scheduleBidTimerExpiration(this.bidTimer)
      }
  }

  def created(): Receive = LoggingReceive {
    case BidTimerExpired =>
      persist(AuctionChangeEvent(Ignored)) {
        event =>
          updateState(event)
          context.parent ! Finished(this.name, empty)
          scheduleDeleteTimerExpiration()
      }
    case Bid(price) =>
      persist(AuctionChangeEvent(Activated(sender, price))) {
        event =>
          updateState(event)
          this.notifier ! Notifier.Notify(this.name, sender, price)
      }
  }

  def ignored(): Receive = LoggingReceive {
    case DeleteTimerExpired =>
      context.stop(self)
    case Start =>
      persist(AuctionChangeEvent(Created(calendar.getTimeInMillis))) {
        event =>
          updateState(event)
          scheduleBidTimerExpiration(this.bidTimer)
      }
  }


  def activated(currentWinner: ActorRef, currentBid: BigInt): Receive = LoggingReceive {
    case Bid(price) if price <= currentBid =>
      sender ! Overbid(currentBid)
    case Bid(price) if !currentWinner.equals(sender) =>
      persist(AuctionChangeEvent(Activated(sender, price))) {
        event =>
          updateState(event)
          currentWinner ! Overbid(price)
          this.notifier ! Notifier.Notify(this.name, sender, price)
      }
    case BidTimerExpired =>
      persist(AuctionChangeEvent(Sold)) {
        event =>
          updateState(event)
          currentWinner ! Won(this.name, currentBid)
          context.parent ! Finished(this.name, Option(currentBid))
          scheduleDeleteTimerExpiration()
      }
  }

  def sold(): Receive = LoggingReceive {
    case DeleteTimerExpired =>
      context.stop(self)
  }

  def scheduleBidTimerExpiration(duration: FiniteDuration): Cancellable = {
    system.scheduler.scheduleOnce(duration, this.self, BidTimerExpired)
  }

  def scheduleDeleteTimerExpiration(): Cancellable = {
    system.scheduler.scheduleOnce(this.deleteTimer, this.self, DeleteTimerExpired)
  }
}
