package lab2

import akka.actor.Actor
import akka.event.LoggingReceive

class AuctionPublisher extends Actor {
  override def receive: Receive = LoggingReceive {
    case n: Notifier.Notify =>
      println("Got message " + n)
  }
}
