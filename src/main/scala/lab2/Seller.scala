package lab2

import akka.actor.{Actor, ActorSystem, Props}
import akka.event.LoggingReceive
import lab2.Seller.Start

import scala.collection.mutable
import scala.concurrent.duration._

object Seller {

  case class Start(auctionNames: Set[String])

}

class Seller() extends Actor {

  val bidTimer = Duration(40, SECONDS)
  val delTimer = Duration(5, MINUTES)
  val system = ActorSystem("Reactive2")

  override def receive: Receive = init

  def init: Receive = LoggingReceive {
    case Start(auctionNames) =>
      auctionNames.foreach(name => {
        val auction = context.actorOf(Props(new Auction(name, bidTimer, delTimer)), "Auction_" + name)
        auction ! Auction.Start
      })
      context become started(auctionNames, mutable.Set.empty)
  }

  def started(startedAuctions: Set[String], finishedAuctions: mutable.Set[String]): Receive = LoggingReceive {
    case Auction.Finished(name, price) =>
      if (price.isEmpty) {
        println("Nobody bought my: " + name)
      } else {
        println("I sold: " + name + " for: " + price.get)
      }
      finishedAuctions += name
      if (finishedAuctions.size == startedAuctions.size) {
        context stop self
      } else {
        context become started(startedAuctions, finishedAuctions)
      }
  }
}
