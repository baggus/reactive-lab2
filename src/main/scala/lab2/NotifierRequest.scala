package lab2

import akka.actor.Actor
import akka.event.LoggingReceive

class NotifierRequest() extends Actor {

  override def receive: Receive = LoggingReceive {
    case n: Notifier.Notify =>
      println("Sending info about:" + n)
      context.actorSelection("akka.tcp://ReactivePublisher@127.0.0.1:9001/user/AuctionPublisher") ! n
      context.stop(self)
  }
}
