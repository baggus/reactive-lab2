package lab2

import akka.actor.SupervisorStrategy.Restart
import akka.actor.{Actor, ActorRef, OneForOneStrategy, Props}
import akka.event.LoggingReceive
import lab2.Notifier.Notify

object Notifier {

  case class Notify(auctionName: String, buyer: ActorRef, price: BigInt)

}

class Notifier extends Actor {

  override def receive: Receive = LoggingReceive {
    case n: Notify =>
      context.actorOf(Props(new NotifierRequest)) ! n
  }

  override val supervisorStrategy = OneForOneStrategy() {
    case _ =>
      println("Restarting...")
      Restart
  }
}
