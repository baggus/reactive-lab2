package lab2

import akka.actor.{Actor, ActorRef}
import akka.event.LoggingReceive
import lab2.AuctionSearch.{Register, Search, SearchResult, Unregister}

import scala.collection.mutable

object AuctionSearch {

  case class Register(name: String)

  case class Unregister(name: String)

  case class Search(name: String)

  case class SearchResult(auctions: List[ActorRef])

}

class AuctionSearch extends Actor {

  val registeredAuctions = new mutable.HashMap[String, ActorRef]

  override def receive: Receive = LoggingReceive {
    case Register(name) =>
      println(self + " Got register request")
      this.registeredAuctions.put(name, sender)
    case Unregister(name) =>
      println(self + " Got unregister request")
      this.registeredAuctions.remove(name)
    case Search(name) =>
      println(self + " Got search request")
      sender ! SearchResult(registeredAuctions.filterKeys(auctionName => auctionName.contains(name)).values.toList)
  }
}
