package lab2

import akka.actor.Actor
import akka.event.LoggingReceive
import lab2.Auction.{Bid, Overbid, Won}
import lab2.AuctionSearch.{Search, SearchResult}
import lab2.Buyer.Start

object Buyer {

  case object Start

}

class Buyer(private val itemName: String, private val budget: BigInt) extends Actor {

  override def receive: Receive = init

  def init: Receive = LoggingReceive {
    case Start =>
      context.actorSelection("akka://Reactive2/user/MasterSearch") ! Search(this.itemName)
      context become waitingForSearchResult
  }

  def waitingForSearchResult: Receive = LoggingReceive {
    case SearchResult(Nil) =>
      println("No auctions matching my request: " + itemName)
      context stop self
    case SearchResult(x :: _) =>
      x ! Bid(budget / 10)
      context become bidding
  }

  def bidding: Receive = LoggingReceive {
    case Overbid(price) if price < this.budget =>
      Thread.sleep(1000)
      println("Bidding for: " + (price + 1))
      sender ! Bid(price + 1)
    case Overbid(price) =>
      println("Price exceeds my budget!")
      context stop self
    case Won(name, price) =>
      println("I bought: " + name + " for " + price)
      context stop self
  }

}
