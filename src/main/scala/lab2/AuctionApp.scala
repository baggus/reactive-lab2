package lab2

import akka.actor.{ActorSystem, Props}
import com.typesafe.config.ConfigFactory

import scala.concurrent.Await
import scala.concurrent.duration._

object AuctionApp extends App {

  val publisherSystem = ActorSystem("ReactivePublisher", ConfigFactory.load("auctionPublisher"))
  val auctionPublisher = publisherSystem.actorOf(Props(new AuctionPublisher), "AuctionPublisher")

  val serverSystem = ActorSystem("Reactive2")

  val auctionSearch = serverSystem.actorOf(Props(new MasterSearch), "MasterSearch")

  val notifier = serverSystem.actorOf(Props(new Notifier), "Notifier")

  val seller1 = serverSystem.actorOf(Props(new Seller()), "seller1")

  seller1 ! Seller.Start(Set("Maluch"))

  val buyer1 = serverSystem.actorOf(Props(new Buyer("Maluch", 550)), "buyer1")
  val buyer2 = serverSystem.actorOf(Props(new Buyer("Maluch", 1000)), "buyer2")

  Thread.sleep(1000)

  buyer1 ! Buyer.Start
  buyer2 ! Buyer.Start

  Await.result(serverSystem.whenTerminated, Duration.Inf)

}
