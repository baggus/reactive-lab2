package lab2

import akka.actor.{Actor, Props}
import akka.event.LoggingReceive
import akka.routing.{ActorRefRoutee, BroadcastRoutingLogic, RoundRobinRoutingLogic, Router}
import lab2.AuctionSearch.{Register, Search, Unregister}

class MasterSearch extends Actor {

  val nbOfroutees: Int = 5

  val routees = Vector.fill(nbOfroutees) {
    val r = context.actorOf(Props[AuctionSearch])
    context watch r
    ActorRefRoutee(r)
  }

  val broadcastRouter = Router(BroadcastRoutingLogic(), routees)

  val roundRobinRouter = Router(RoundRobinRoutingLogic(), routees)


  override def receive: Receive = LoggingReceive {
    case m: Register =>
      this.broadcastRouter.route(m, sender())
    case m: Unregister =>
      this.broadcastRouter.route(m, sender())
    case m: Search =>
      this.roundRobinRouter.route(m, sender())
  }
}
